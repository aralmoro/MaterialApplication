package com.stratpoint.android.materialapplication.fragments;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.stratpoint.android.materialapplication.R;

/**
 * Created by angelaalmoro on 2/29/16.
 */
public class TabFragment2 extends Fragment implements View.OnClickListener {
    private Button snackbarBtn1;
    private Button snackbarBtn2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab2, container, false);

        snackbarBtn1 = (Button) view.findViewById(R.id.snackbar1);
        snackbarBtn2 = (Button) view.findViewById(R.id.snackbar2);

        snackbarBtn1.setOnClickListener(this);
        snackbarBtn2.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        if (v == snackbarBtn1) {
            Snackbar.make(v, getString(R.string.snackbar_text), Snackbar.LENGTH_LONG)
                    .show();
        } else if (v == snackbarBtn2) {
            Snackbar.make(v, getString(R.string.snackbar_text), Snackbar.LENGTH_LONG)
                    .setAction(getString(R.string.dismiss), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    })
                    .show();
        }
    }
}
