package com.stratpoint.android.materialapplication.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.stratpoint.android.materialapplication.fragments.TabFragment1;
import com.stratpoint.android.materialapplication.fragments.TabFragment2;

/**
 * Created by angelaalmoro on 2/29/16.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    int mTabCount;

    public PagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.mTabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                TabFragment1 tab1 = new TabFragment1();
                return tab1;

            case 1:
                TabFragment2 tab2 = new TabFragment2();
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mTabCount;
    }
}
