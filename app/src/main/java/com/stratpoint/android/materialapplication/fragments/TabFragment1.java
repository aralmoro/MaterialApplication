package com.stratpoint.android.materialapplication.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.stratpoint.android.materialapplication.R;

/**
 * Created by angelaalmoro on 2/29/16.
 */
public class TabFragment1 extends Fragment implements View.OnClickListener {
    private EditText passwordEdittext;
    private TextInputLayout passwordLayout;
    private Button submitButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab1, container, false);

        passwordEdittext = (EditText) view.findViewById(R.id.password_ET);
        passwordLayout = (TextInputLayout) view.findViewById(R.id.password_layout);
        passwordLayout.setErrorEnabled(true);

        submitButton = (Button) view.findViewById(R.id.submit_btn);
        submitButton.setOnClickListener(this);

        return view;
    }

    private boolean validateData() {
        boolean res;

        if(passwordEdittext.getText().toString().length() < 8) {
            passwordLayout.setError(getString(R.string.invalid_password));
            res = false;
        } else {
            passwordLayout.setError("");
            res = true;
        }

        return res;
    }

    @Override
    public void onClick(View v) {
        if (v == submitButton) {
            if (validateData()) {
                Snackbar.make(v, getString(R.string.success), Snackbar.LENGTH_LONG)
                        .show();
                closeKeyboard(getActivity(), passwordEdittext.getWindowToken());
            }
        }
    }

    private void closeKeyboard(Context c, IBinder windowToken) {
        InputMethodManager mgr = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(windowToken, 0);
    }

}
